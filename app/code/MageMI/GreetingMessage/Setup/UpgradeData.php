<?php 

namespace MageMI\GreetingMessage\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @inheritDoc
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.0', '<')) {
            $setup->getConnection()->update(
                $setup->getTable('greeting_message'),
                [
                    'description' => 'This is description information for being update'
                ],
                $setup->getConnection()->quoteInto('greeting_id = ?', 1 )
            );
            $setup->endSetup();
        }
    }
}

;?>