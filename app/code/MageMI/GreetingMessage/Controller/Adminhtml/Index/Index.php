<?php 

namespace MageMI\GreetingMessage\Controller\Adminhtml\Index;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;

class Index extends Action
{
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Raw $result */

        //  $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        //  $result->setContents("Hello Admin!");
        //  return $result;

        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}
;?>