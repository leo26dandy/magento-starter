<?php

namespace MageMI\GreetingMessage\Controller\Index;

use MageMI\GreetingMessage\Model\ItemFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Booking extends \Magento\Framework\App\Action\Action
{
    /**
     * Booking action
     *
     * @return void
     */

    private $itemFactory;

    public function __construct(
        Context $context,
        ItemFactory $itemFactory
    ){
        $this->itemFactory = $itemFactory;
        parent::__construct($context);    
    }

    public function execute()
    {
        // 1. POST request : Get booking data
        $post = (array) $this->getRequest()->getPost();

        if (!empty($post)) {
            // Retrieve your form data
            $message     = $post['message'];
            $description = $post['description'];

            // Doing-something with...
            $item = $this->itemFactory->create();
            $item->setMessage($message);
            $item->setDescription($description);
            $item->save();

            // Display the succes form validation message
            $this->messageManager->addSuccessMessage('Add message done!');

            // Redirect to your form page (or anywhere you want...)
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl('/form/index/booking');

            return $resultRedirect;
        }
        // 2. GET request : Render the booking page 
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}