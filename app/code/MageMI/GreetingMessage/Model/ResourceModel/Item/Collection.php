<?php 

namespace MageMI\GreetingMessage\Model\ResourceModel\Item;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use MageMI\GreetingMessage\Model\Item;
use MageMI\GreetingMessage\Model\ResourceModel\Item as ItemResource;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'greeting_id';

    protected function _construct()
    {
        $this->_init(Item::class, ItemResource::class);
    }
}
;?>