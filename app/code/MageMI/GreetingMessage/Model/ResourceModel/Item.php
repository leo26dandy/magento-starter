<?php 

namespace MageMI\GreetingMessage\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Item extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('greeting_message', 'greeting_id');
    }
}
;?>