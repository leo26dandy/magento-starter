<?php

namespace MageMI\GreetingMessage\Observer;

use Magento\Framework\Event\ObserverInterface;
use MageMI\GreetingMessage\Helper\Data;
use Magento\Framework\Event\Observer;

class UpdateLayoutObserver implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getProduct();
        $sku     = $product->getSku();
        $id      = $product->getId();
        $name    = $product->getName();
    }
}