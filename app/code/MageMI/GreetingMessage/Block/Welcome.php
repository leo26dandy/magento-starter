<?php 

namespace MageMI\GreetingMessage\Block;

use Magento\Framework\View\Element\Template;
use MageMI\GreetingMessage\Model\ResourceModel\Item\Collection;
use MageMI\GreetingMessage\Model\ResourceModel\Item\CollectionFactory;

class Welcome extends Template
{
    private $collectionFactory;

    public function __construct(
        Template\Context $context, 
        CollectionFactory $collectionFactory,   
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return \MageMI\GreetingMessage\Model\Item[]
     */
    
    public function getItems()
    {
        return $this->collectionFactory->create()->getItems();
    }
}
;?>