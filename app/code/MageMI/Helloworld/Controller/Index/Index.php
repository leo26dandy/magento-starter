<?php 

namespace MageMI\Helloworld\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;

class Index extends Action
{

    public function execute()
    {
        /** 
         * @var \Magento\Framework\View\Result\Page $resultPage 
        */
        
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        return $resultPage;
    }
}

;?>